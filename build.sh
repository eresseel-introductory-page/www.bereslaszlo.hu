#!/bin/sh

sed 's#http://www.bereslaszlo#https://www.bereslaszlo.hu#g' -i ./index.html ./lang/en/* ./lang/hu/*

# mkdir -p ./output/css
# mkdir -p ./output/js
# mkdir -p ./output/html/lang/en
# mkdir -p ./output/html/lang/hu

# minify ./index.html > ./output/html/index.html
# minify ./lang/en/index.html > ./output/html/lang/en/index.html
# minify ./lang/hu/index.html > ./output/html/lang/hu/index.html

# CSS=$(find ./assets/css/ -name "*.css" | cut -d"/" -f4)
# for i in ${CSS}; do
#   echo ${i}
#   minify ./assets/css/${i} > ./output/css/${i}
# done

# JS=$(find ./assets/js/ -name "*.js" | cut -d"/" -f4)
# for i in ${JS}; do
#   echo ${i}
#   minify ./assets/js/${i} > ./output/js/${i}
# done

# purifycss ./lang/hu/index.html ${CSS} ${JS}  --info --out ./output/css/style.min.css

# cp -R ./output/css/* ./assets/css/
# cp -R ./output/js/* ./assets/js/
# cp -R ./output/html/index.html .
# cp -R ./output/html/lang/en/* ./lang/en/
# cp -R ./output/html/lang/hu/* ./lang/hu/

echo "DONE"
