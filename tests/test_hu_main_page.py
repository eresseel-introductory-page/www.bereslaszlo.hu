import unittest
import pathlib
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options

class Bereslaszlo(unittest.TestCase):

    def setUp(self):
        options = Options()
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-gpu')
        options.add_argument("headless")
        options.add_argument("--window-size=1920,1080")
        self.driver = webdriver.Chrome(options=options)


    def test_title(self):
        driver = self.driver
        driver.get(f"file:///{pathlib.Path().resolve()}/index.html")

        self.assertIn("Béres László | CV", driver.title)


    def test_welcome_message(self):
        driver = self.driver
        driver.get(f"file:///{pathlib.Path().resolve()}/lang/hu/index.html")

        contents = driver.find_element(by=By.CSS_SELECTOR, value='h1.header-title')
        self.assertIn("Béres László", contents.text)

        contents = driver.find_element(by=By.CSS_SELECTOR, value='h6.header-mono')
        self.assertIn("Senior Linux | Mágus", contents.text)


    def test_menu_items(self):
        driver = self.driver
        driver.get(f"file:///{pathlib.Path().resolve()}/lang/hu/index.html")

        items = ["Bemutatkozás", "Rólam", "Önéletrajz", "Projektek", "Blog", "Kapcsolat"]
        menu_items = driver.find_element(by=By.ID, value='navbarSupportedContent')

        for item in items:
            self.assertIn(item, menu_items.text)


    def test_about_me_section(self):
        driver = self.driver
        driver.get(f"file:///{pathlib.Path().resolve()}/lang/hu/index.html")

        titles = ["Ki vagyok én?", "Személyes adatok", "Tapasztalatok"]
        about_me_contents= ["Egy csodálatos gyerek boldog apukája",
                            "Budapesten élek a családommal, ahol a szürke hétköznapok mellett igyekszünk minél több időt együtt tölteni. Hétvégéken eljárunk kirándulni, sétálni a városba, vagy csak egyszerűen lemenni a közeli játszótérre.",
                            "A szabadidőm egy részét a gyerekeimmel való közös játékok és programok teszik ki. Amikor nem a családommal vagyok akkor tanulok, hogy nem maradjak le, és részt veszek különféle meetup-on, ahol érdekes emberekkel ismerkedem meg és új dolgokat tanulok.",
                            "Emellett az egyik hobbim a régi autók szerelése, ami igazán kikapcsol és feltölt.",
                            "CV Letöltése"]
        personal_datas = ["Születési év: 1990",
                          "E-mail: beres.laszlo1990@gmail.com",
                          "Telefonszám: +36-70/882-45-44",
                          "Lakhely: Budapest"]
        experiences_title = ["DevOps", "Programozás", "Build tool, Tesztelés", "Tervezés"]
        experiences_content = ["AWS, GCP, Azure, Nginx, Traefik",
                               "Kubernetes, Docker, Swarm, ContainerD",
                               "Ansible, Helm, Terraform, Packer, Graylog",
                               "Jenkins, GitLab, Prometheus, Grafana",
                               "HTML, PHP, CSS, JS, CI3",
                               "Java, Groovy, Bash, Python",
                               "Testinfra, Molecule",
                               "Maven, Gradle, Selenium",
                               "Hálózat, Proxmox, Vagrant, Qemu"]

        contents = driver.find_element(by=By.ID, value='rolam')

        for title in titles:
            self.assertIn(title, contents.text)

        for content in about_me_contents:
            self.assertIn(content, contents.text)

        for personal_data in personal_datas:
            self.assertIn(personal_data, contents.text)

        for experience in experiences_title:
            self.assertIn(experience, contents.text)

        for experience in experiences_content:
            self.assertIn(experience, contents.text)


    def test_cv(self):
        driver = self.driver
        driver.get(f"file:///{pathlib.Path().resolve()}/lang/hu/index.html")

        items = ["Munkahelyek", "Tanulmányok", "OS Ismeretek", "Programozás", "Nyelvismeret"]
        cv_items = driver.find_element(by=By.ID, value='cv')

        self.assertIn("Önéletrajz", cv_items.text)

        for item in items:
            self.assertIn(item, cv_items.text)


    def test_properties(self):
        driver = self.driver
        driver.get(f"file:///{pathlib.Path().resolve()}/lang/hu/index.html")

        items = ["Kitartás", "Precizitás", "Csapatmunka",
                 "Megbízhatóság", "Kreativitás", "Önnálló munkavégzés"]
        properties_items = driver.find_element(by=By.ID, value='skills')

        self.assertIn("Képességek", properties_items.text)

        for item in items:
            self.assertIn(item, properties_items.text)


    def test_contact_me(self):
        driver = self.driver
        driver.get(f"file:///{pathlib.Path().resolve()}/lang/hu/index.html")

        items = ["Szívesen vállalok kihívásokkal teli projekteket, mert ezek lehetőséget adnak a fejlődésre és az új tapasztalatok megszerzésére!"]
        contact_me_items = driver.find_element(by=By.CSS_SELECTOR, value='h2.text-light')

        for item in items:
            self.assertIn(item, contact_me_items.text)


    def test_projects(self):
        driver = self.driver
        driver.get(f"file:///{pathlib.Path().resolve()}/lang/hu/index.html")

        items = ["Web", "Docker", "Programozás", "DevOps"]
        projects_items = driver.find_element(by=By.ID, value='projektek')

        self.assertIn("Projektek", projects_items.text)

        for item in items:
            self.assertIn(item, projects_items.text)


    def test_blog(self):
        driver = self.driver
        driver.get(f"file:///{pathlib.Path().resolve()}/lang/hu/index.html")

        blog_items = driver.find_element(by=By.ID, value='blog')

        self.assertIn("Legfrissebb Bejegyzések", blog_items.text)


    def test_contact(self):
        driver = self.driver
        driver.get(f"file:///{pathlib.Path().resolve()}/lang/hu/index.html")

        contact_items = driver.find_element(by=By.ID, value='kapcsolat')

        self.assertIn("Kapcsolat felvétel", contact_items.text)


    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()