# www.bereslaszlo.hu

## Prepare development environment
```bash
vagrant up www-bereslaszlo-hu
```
## Prepare test environment
```bash
python3 -m venv .venv
source .venv/bin/activate

pip3 install -r test-requirements.txt
python tests/test_hu_main_page.py
```